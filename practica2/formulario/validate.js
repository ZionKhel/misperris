$(function() {
	
	  $("#formulario-adopcion").validate({
		rules: {
		  correo: {
			required: true,
			correo: true
		  },
		  run:{
			required: true,
			run: true
		  },
		  Nombre:{
			required: true,
			Nombre: true
		  },
		  Fechanac:{
			required: true,
			max: "2000-12-31"

		  },
		  Telefono:{
			required: false
		  },
		  Region:{
			required:true
			},
			Ciudad:{
				required:true
				},
			Vivienda:{
				required:true
				},
				
		},
		messages: {
		  correo: {
			required: 'Ingresa tu correo electrónico',
			email: 'Formato de correo no válido'
		  },
		  Run: {
			required: 'Ingresa tu rut',
			run: 'Formato no valido'
		  },
		  Nombre: {
			required: 'Ingresa tu nombre completo'
		  },
		  Fechanac: {
			required: 'Ingresa tu fecha de cumpleaños',
			max: 'Debe ser anterior al 2001'
		  },
		  Region: {
			required: 'Selecciona una opción'
		  },
	
		  }
		});
	});